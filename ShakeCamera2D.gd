extends Camera2D

export var decay = 0.8
export var max_offset = Vector2(50, 40)
export var max_roll = 0.1
export(float) var noise_period = 4
export(int) var noise_octaves = 2
export(float) var noise_speed = 10

export(NodePath) var target

onready var noise = OpenSimplexNoise.new()
var noise_y = 0

var trauma = 0
var trauma_exponent = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	noise.seed = randi()
	noise.period = noise_period
	noise.octaves = noise_octaves
	$"../SnakeSpawner".connect("spawned_snake", self, "_on_snake_spawned")
	pass # Replace with function body.

func _on_snake_spawned(snake):
	snake.connect("hit_wall", self, "_on_snake_hit_wall")

func _on_snake_hit_wall(position):
	# add_trauma(0.1)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if trauma:
		trauma = max(0, trauma - decay * delta)
		shake(delta)
	pass

func shake(delta : float):
	noise_y += noise_speed * delta
	var amount = pow(trauma, trauma_exponent)
	rotation = max_roll * amount * noise.get_noise_2d(noise.seed, noise_y)
	offset.y = max_offset.y * amount * noise.get_noise_2d(noise.seed * 2, noise_y)
	offset.x = max_offset.x * amount * noise.get_noise_2d(noise.seed * 3, noise_y)

func add_trauma(amount : float):
	trauma = min(amount + trauma, 1)
