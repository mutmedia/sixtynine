extends Node2D
export var arc_start = 0
export var arc_direction = 1
export var arc_point_count = 10
export var arc_radius = 13
export var arc_color = Color.red
export var arc_thickness = 2

export var period = 1
export var cooldown = 0.2
export(Curve) var timer_curve

signal timer_over

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _draw():
	var i = timer_curve.interpolate(timer / period)
	if timer <= 0:
		pass
	draw_arc(Vector2.ZERO, arc_radius, deg2rad(arc_start), deg2rad(arc_start) + sign(arc_direction) * i * 2 * PI, arc_point_count, arc_color, arc_thickness);

# Called every frame. 'delta' is the elapsed time since the previous frame.
var timer = 0
func _process(delta):
	timer += delta
	if timer > period:
		timer = -cooldown
		emit_signal("timer_over")
	update()
	pass
