extends Node2D
export var move_speed = 5
export var initial_size = 500
export var thickness = 10
export(Color) var color = Color.blue
export(NodePath) var divided_space_path

var divided_space = null

signal hit_wall(position)

var body_memory = [] #most recent memory in front
onready var size = initial_size
onready var move_direction = [Vector2(1, 1), Vector2(1, -1), Vector2(-1, 1), Vector2(-1, -1)][randi()%4].normalized()


# Called when the node enters the scene tree for the first time.
func _ready():

	pass # Replace with function body.

func _draw():
	var size_remaining = size
	var direction = move_direction
	var cur_position = position
	var max_i = 0
	for i in range(0, body_memory.size()):
		max_i = i
		var mem = body_memory[i]
		var mem_distance = cur_position.distance_to(mem.position)
		if mem_distance <= size_remaining:
			draw_line(cur_position-position, mem.position-position, color, thickness)
			size_remaining -= mem_distance
			direction = mem.direction
			cur_position = mem.position
			# draw_circle(mem.position-position, 10, Color.red)
		else:
			break

	for i in range(max_i + 1, body_memory.size()):
		body_memory.remove(i)
		continue

	draw_line(cur_position - position, cur_position-direction * size_remaining - position, color, thickness)
	# draw_circle(Vector2.ZERO, 10, Color.green)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var move_remaining = move_speed * delta
	var current_sub = divided_space.get_subdivision_rect(position)
	while move_remaining > 0:
		var tentative_next_position = position + move_remaining * move_direction
		var intersect_x
		var intersect_y
		if move_direction.x > 0 and tentative_next_position.x >= current_sub.end.x:
			intersect_x = current_sub.end.x
		if move_direction.x < 0 and tentative_next_position.x <= current_sub.position.x:
			intersect_x = current_sub.position.x
		if move_direction.y > 0 and tentative_next_position.y >= current_sub.end.y:
			intersect_y = current_sub.end.y
		if move_direction.y < 0 and tentative_next_position.y <= current_sub.position.y:
			intersect_y = current_sub.position.y

		if intersect_x == null and intersect_y == null:
			position = tentative_next_position
			break
		
		var intersection
		var is_x_intersection = intersect_y == null or (intersect_x != null and abs(position.x - intersect_x) < abs(position.y - intersect_y))

		if is_x_intersection:
			intersection = position + move_direction * (intersect_x - position.x) / move_direction.x
		else:
			intersection = position + move_direction * (intersect_y - position.y) / move_direction.y

		body_memory.push_front({
			position = intersection,
			direction = move_direction
		})

		move_remaining -= (position - intersection).length()
		position = intersection

		emit_signal("hit_wall", position)

		if is_x_intersection:
			move_direction.x = -move_direction.x
		else:
			move_direction.y = -move_direction.y

	update()
	pass
