extends Polygon2D

var shattered = false
var animate_shards = false


var shake_fraction = 0.5
var exponential_depth_based_power_decrease = 0.85

var pos = 0.0
var speed = 0.0
var depth = 0

var first_direction = Vector2.RIGHT
var first_shard
var first_id


var second_direction = Vector2.LEFT
var second_shard
var second_id

var time_untill_mended = 1.0

func _ready():
	add_to_group("shards")
	pass



func _process(delta):
	if not shattered:
#		self_modulate = lerp(Color(randf(),randf(),randf()), Color.white, 0.0)
		pass
	if shattered:
		if animate_shards:
			
			speed = lerp(speed, (-pos)*20, delta * 5)
			
			pos += delta * speed
			
			pos = max(pos, 0.0)
			
			if is_a_parent_of(first_shard):
				first_shard.position = first_direction * pos + Vector2.RIGHT.rotated(randf()*TAU) * shake_fraction * pos
			if is_a_parent_of(second_shard):
				second_shard.position = second_direction * pos + Vector2.RIGHT.rotated(randf()*TAU) * shake_fraction * pos
		
		time_untill_mended -= delta
		if time_untill_mended < 0 or pos < 1:
			try_mend()
			time_untill_mended = 0.0
			pos = 0.0
	pass

func split(power):
	if get_child_count() == 1:
		# there is one kid, nothing visibly split, revert
		try_mend()
		shattered = true
		animate_shards = false
		time_untill_mended = 0.0
	elif get_child_count() == 2:
		# visibly shattered, procede as usual
		shattered = true
		animate_shards = true
		self_modulate = Color.transparent
		remove_from_group("shards")
		time_untill_mended = 1.0
		
		first_shard = get_child(0)
		second_shard = get_child(1)
		
		var first_com = Vector2.ZERO
		for point in first_shard.polygon:
			first_com += point
		first_com /= first_shard.polygon.size()
		
		var second_com = Vector2.ZERO
		for point in second_shard.polygon:
			second_com += point
		second_com /= second_shard.polygon.size()
		
		var to_second = (second_com - first_com).normalized()
		
		first_direction = to_second.rotated(TAU/2)
		second_direction = to_second
		
		pos = 10
		speed = 100*power * pow(exponential_depth_based_power_decrease,depth)

	else:
		# what the fuck did you do, how did we get here???
#		print("shard splitting shat the bed")
		# this print is funny but causes output console to overflow so its removed for now
#		print("                      @@@@@@                                            \n				 .@@@#      /@@@                                               \n				@@.        (@@(*@@                                             \n			  /@@         @,#@   @@*                                           \n			  @@   @     @ %@     @@                                           \n			  @@ .@. &@.@ *@      @@                                           \n			  @@    @@   #@       @@     #@,                                   \n			   @@%    @@@@       @@    @            &                          \n				 @@@          *@@,    @              *                         \n				   .@@@@@@@@@@%      @                @                        \n						  #@@        @                @                        \n						   @@.       ,@              @                         \n						   @ @         @            @                          \n						   @  @          @       @@    @ #                     \n						   .@ /@@   #@. @/.....@...@@(@@ .&                    \n						   @@@@@/ @,             @@   *@@@@,                   \n						   @@@@@@    @@,        @@@@  @@@@@                    \n						   /@@  @   @@@            @ @@@#                      \n							 %@ @@&  @             @.                          \n									&#              @                          \n									@               @                          \n								   ,@               @                          \n								   ,@      @@       @                          \n								   ,@     @@@@@      #                         \n								   ,@     &@ @@@     @                         \n									@     @@  @@     *@                        \n									@     @@   @@     @                        \n									@     .@   @(     @                        \n			PIZDETS              ")
		pass
	pass

func mend():
	shattered = false
	animate_shards = false
	self_modulate = Color.white
	add_to_group("shards")
	for child in get_children():
		child.queue_free()
	pass

func try_mend():
	var can_mend = true
	
	for child in get_children():
		if child.shattered == true:
			can_mend = false
	
	if can_mend:
		mend()
	pass
