extends Node2D
class_name SpaceDivision
onready var space_division = SpaceSubdivision.new(Vector2.ZERO, Direction.vertical, get_viewport_rect())

enum Direction {
	horizontal,
	vertical
}

class SpaceSubdivision:
	var position : Vector2
	var direction
	var limits : Rect2

	var subdivision_lt = null 
	var subdivision_gt = null 

	func _init(position: Vector2, direction, limits):
		self.position = position
		self.direction = direction
		self.limits = limits

	var has_subdivisions = false setget , get_has_subdivisions
	func get_has_subdivisions():
		return subdivision_lt != null || subdivision_gt != null

	func tlbr_to_rect2(tlbr):
		return Rect2(tlbr.left, tlbr.top, tlbr.right-tlbr.left, tlbr.bottom-tlbr.top);
	
	func limits_lt():
		return tlbr_to_rect2({
			top = self.limits.position.y,
			left = self.limits.position.x,
			bottom = self.limits.end.y if self.direction == Direction.vertical else position.y,
			right = self.limits.end.x if self.direction == Direction.horizontal else position.x,
		})

	func limits_gt():
		return tlbr_to_rect2({
			top = self.limits.position.y if self.direction == Direction.vertical else position.y,
			left = self.limits.position.x if self.direction == Direction.horizontal else position.x,
			bottom = self.limits.end.y,
			right = self.limits.end.x,
		})

	func get_subdivision_rect(pos : Vector2):
		var compA
		var compB
		if direction == Direction.vertical:
			compA = pos.x
			compB = position.x
		else:
			compA = pos.y
			compB = position.y

		if compA < compB:
			if subdivision_lt != null:
				return subdivision_lt.get_subdivision_rect(pos)
			else:
				return self.limits_lt()
		else:
			if subdivision_gt != null:
				return subdivision_gt.get_subdivision_rect(pos)
			else:
				return self.limits_gt()

	func subdivide(new_position : Vector2, new_direction):
		var compA
		var compB
		if direction == Direction.vertical:
			compA = new_position.x
			compB = position.x
		else:
			compA = new_position.y
			compB = position.y
		
		if compA < compB:
			if subdivision_lt == null:
				subdivision_lt = SpaceSubdivision.new(new_position, new_direction, limits_lt())
			else:
				subdivision_lt.subdivide(new_position, new_direction)
		else:
			if subdivision_gt == null:
				subdivision_gt = SpaceSubdivision.new(new_position, new_direction, limits_gt())
			else:
				subdivision_gt.subdivide(new_position, new_direction)

var current_direction = Direction.vertical
var cursor_position = Vector2.ZERO


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	pass # Replace with function body.

func _draw():
	var queue = [space_division]
	var res = get_viewport_rect().size

	var i = 10
	var min_line_width = 2
	var max_line_width = 6
	while queue.size() > 0:
		var next = queue.pop_front()
		if next == null:
			continue
		i = i + 0.5
		i = i - floor(i)

		var rect_hue = 0.25 + randf() * 0.65
		var rect_sat = 0.8
		var rect_val = 0.7
		var rect_a = 0.3
		var rect_color = Color.from_hsv(rect_hue, rect_sat, rect_val, rect_a)
		draw_rect(next.limits, rect_color)

		var line_width = min_line_width + (max_line_width - min_line_width) * randf()
		if next.direction == Direction.horizontal:
			draw_line(Vector2(next.limits.position.x, next.position.y), Vector2(next.limits.end.x, next.position.y), Color(255, 0, 255), line_width)
		else:
			draw_line(Vector2(next.position.x, next.limits.position.y), Vector2(next.position.x, next.limits.end.y), Color(255, 0, 255), line_width)

		if next.has_subdivisions:
			queue.push_back(next.subdivision_lt)
			queue.push_back(next.subdivision_gt)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func subdivide_space(position : Vector2, direction):
	var res = get_viewport_rect().size
	var limits = Rect2(0, 0, res.x, res.y)

	var next = space_division
	if next == null:
		space_division = SpaceSubdivision.new(position, direction, limits)
	else:
		space_division.subdivide(position, direction)
	update()

func get_subdivision_rect(position : Vector2):
	return space_division.get_subdivision_rect(position)


func _process(delta):
	pass

func _input(event):
	# Mouse in viewport coordinates.
	# subdivide_space(cursor_position, current_direction)
	pass

func _on_Cursor_cursor_input(position, direction):
	subdivide_space(position, direction)
