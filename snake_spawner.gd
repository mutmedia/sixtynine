extends Node2D
var snake_scene = preload("res://snake.tscn")
export(NodePath) var divided_space_path
onready var divided_space = get_node(divided_space_path)

signal spawned_snake(snake_obj)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func spawn_snake_rand():
	var view = get_viewport_rect()
	spawn_snake(Vector2(view.size.x * randf(), view.size.y * randf()))

func spawn_snake(pos : Vector2):
	var new_snake = snake_scene.instance()
	new_snake.position = pos
	new_snake.divided_space = divided_space
	add_child(new_snake)
	emit_signal("spawned_snake", new_snake)

func _input(event):
	if event is InputEventMouseButton and event.pressed:
		spawn_snake_rand()
