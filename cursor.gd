extends Node2D
export  var cursor_size = 10
export  var cursor_width = 4
export  var cursor_color = Color(0, 0, 0)

var current_direction = 0

signal cursor_input(position, direction)

func send_input():
	emit_signal("cursor_input", position, current_direction)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _draw():
	# cursor stuff
	var offset = cursor_size * (Vector2.UP if current_direction == SpaceDivision.Direction.vertical else Vector2.RIGHT)
	draw_line(offset, -offset, cursor_color, cursor_width)
	pass

func _process(delta):
	var mouse_position = get_viewport().get_mouse_position()
	global_position = mouse_position
	update()

# func _input(event):
# 	if event is InputEventKey:
# 		if event.pressed and event.scancode == KEY_SPACE:
# 			toggle_direction()
# 	if event is InputEventMouseButton and event.pressed:
# 		send_input()


func toggle_direction():
	current_direction = SpaceDivision.Direction.horizontal if current_direction == SpaceDivision.Direction.vertical else SpaceDivision.Direction.vertical

func _on_cursor_timer_timer_over():
	send_input()
	toggle_direction()
	pass # Replace with function body.
